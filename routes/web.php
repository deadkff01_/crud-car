<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home'); 

Route::group(['prefix' => 'user', 'middleware' => ['auth']], function ($app) {
   Route::resource('user', 'UserController');
   Route::get('/', 'UserController@index'); 
   Route::get('/create', ['uses' => 'UserController@create', 'as' => 'user.create']);
   Route::put('/destroy', 'UserController@destroy');
   Route::get('/detail', ['uses' => 'UserController@detail', 'as' => 'user.detail']); 

   // cars
   Route::get('/cars', ['uses' => 'CarController@index', 'as' => 'cars']);
   Route::get('/addCar','CarController@create')->name('addCar');
   Route::post('addCarForm', ['uses' => 'CarController@store', 'as' => 'addCarForm']); 

   Route::get('edit/{id}', ['uses' => 'CarController@edit', 'as' => 'edit']);
   Route::post('edit/{id}','CarController@update');
   Route::delete('{id}','CarController@destroy');

   // company
   Route::get('/company', ['uses' => 'CompanyController@index', 'as' => 'company']);
   Route::get('/addCompany','CompanyController@create')->name('addCompany');
   Route::post('addCompanyForm', ['uses' => 'CompanyController@store', 'as' => 'addCompanyForm']); 

   Route::get('editCompany/{id}', ['uses' => 'CompanyController@edit', 'as' => 'editCompany']);
   Route::post('editCompany/{id}','CompanyController@update');
   Route::delete('deleteCompany/{id}','CompanyController@destroy');
}); 