<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;
use App\Company;

class CompanyController extends Controller {
    public function index() {
        $companies = Company::all();
        return view('company/index', compact('companies'));
    }

    public function create() {
        return view('company.create');
    }

    public function store(Request $request) {
        $company = new Company();
        $company->name = $request->get('name');       
        $company->save();
        return redirect('user/company')->with('success', 'Company has been successfully added');
    }

    public function edit($id) {
        $company = Company::find($id);
        return view('company.edit', compact('company','id'));
    }

    public function update(Request $request, $id) {
        $company= Company::find($id);
        $company->name = $request->get('name');    
        $company->save();
        return redirect('user/company')->with('success', 'Company has been successfully update');
    }

    public function destroy($id) {
        $company = Company::find($id);
        $company->delete();
        return redirect('user/company')->with('success','Company has been  deleted');
    }
}
