<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;
use App\Company;

class CarController extends Controller {
    public function index() {
        $cars = Car::all();
        return view('car/index', compact('cars'));
    }

    public function create() {
        $companies = Company::all();
        return view('car.create', ['companies' => $companies]);
    }

    public function store(Request $request) {
        $car = new Car();
        $car->model = $request->get('model');
        $car->price = $request->get('price');
        $company = Company::find($request->get('company'));
        $car->company = $company['attributes'];
        $car->save();
        return redirect('user/cars')->with('success', 'Car has been successfully added');
    }

    public function edit($id) {
        $car = Car::find($id);
        $companies = Company::all();
        $data = array('car' => $car,
                      'id'=> $id,
                      'companies' => $companies);
        return view('car.edit')->with($data);
    }

    public function update(Request $request, $id) {
        $car= Car::find($id);
        $company = Company::find($request->get('company'));
        $car->company = $company['attributes'];
        $car->model = $request->get('model');
        $car->price = $request->get('price');        
        $car->save();
        return redirect('user/cars')->with('success', 'Car has been successfully update');
    }

    public function destroy($id) {
        $car = Car::find($id);
        $car->delete();
        return redirect('user/cars')->with('success','Car has been  deleted');
    }
}
