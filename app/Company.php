<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Company extends Eloquent {
    protected $connection = 'mongodb';
    protected $collection = 'company';
    
    protected $fillable = [
        'name'
    ];
}