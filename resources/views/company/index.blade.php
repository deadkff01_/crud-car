@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Company Crud</div>
                <div class="panel-body">
                <a class="btn btn-default" style="float:right" href="{{ route('addCompany') }}" type="submit">Add Company</a>
                {{csrf_field()}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th colspan="0" style="width: 146px;text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($companies as $company)
                    <tr>
                        <td>{{$company->id}}</td>
                        <td>{{$company->name}}</td>
                        
                        <td style="text-align: center;">
                        <a href="{{action('CompanyController@edit', $company->id)}}" style="float: left;" class="btn btn-warning">Edit</a>
                        <form action="{{action('CompanyController@destroy', $company->id)}}" method="post">
                        {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection