@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create Car</div>
                <div class="panel-body">
                <form method="post" action="{{ route('addCarForm')}}">
                {{csrf_field()}}

                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                      <label for="Carcompany">Car Company:</label>
                      <select id="company" name="company" class="form-control">
                      @foreach ($companies as $c)
                        <option value="{{ $c->_id }}">{{ $c->name }}</option>
                      @endforeach
                  </select>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                      <label for="Model">Model:</label>
                      <input type="text" class="form-control" name="model">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                      <label for="Price">Price:</label>
                      <input type="text" class="form-control" name="price">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                      <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                  </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection