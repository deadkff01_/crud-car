@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Car Crud</div>
                <div class="panel-body">
                <a class="btn btn-default" style="float:right" href="{{ route('addCar') }}" type="submit">Add Car</a>
                {{csrf_field()}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company</th>
                        <th>Model</th>
                        <th>Price</th>
                        <th colspan="0" style="width: 146px;text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    @foreach($cars as $car)
                    <tr>
                        <td>{{$car->id}}</td>
                        <td>{{$car->company['name']}}</td>
                        <td>{{$car->model}}</td>
                        <td>{{number_format($car->price, 2, ',', '.')}}</td>
                        <td style="text-align: center;">
                        <a href="{{action('CarController@edit', $car->id)}}" style="float: left;" class="btn btn-warning">Edit</a>
                        <form action="{{action('CarController@destroy', $car->id)}}" method="post">
                        {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection